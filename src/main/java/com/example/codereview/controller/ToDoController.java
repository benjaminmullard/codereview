package com.example.codereview.controller;

import com.example.codereview.dto.TodoDTO;
import com.example.codereview.model.Todo;
import com.example.codereview.model.TodoItem;
import com.example.codereview.repository.ToDoRepository;
import com.example.codereview.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/todos/", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class ToDoController {

    @Autowired
    private ToDoService toDoService;

    @Autowired
    private ToDoRepository toDoRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Todo> createTodo(@RequestBody final Todo todo) {

        final var persistedTodo = toDoService.createTodo(todo);

        return ResponseEntity.created(UriComponentsBuilder.fromHttpUrl("/todos/{0}/")
                                                          .build(persistedTodo.getCreatedTimestamp()))
                             .body(persistedTodo);
    }

    @PostMapping("{todoId}/")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<TodoItem> createTodoItem(@PathVariable final UUID todoId,
                                                   @RequestBody final TodoItem todoItem) {

        final var persistedTodoItem = toDoService.createTodoItem(todoId, todoItem);

        return ResponseEntity.created(UriComponentsBuilder.fromHttpUrl("/todos/{0}/items/{1}")
                                                          .build(todoId, persistedTodoItem.getId()))
                             .body(persistedTodoItem);
    }

    @GetMapping
    public ResponseEntity<List<TodoDTO>> getTodos() {

        return ResponseEntity.ok(toDoRepository.getTodoList()
                                               .stream()
                                               .map(Todo::toDTO)
                                               .collect(Collectors.toList()));
    }

    @GetMapping("{todoId}/")
    public ResponseEntity<Todo> getTodo(@RequestParam final UUID todoId) {

        return ResponseEntity.ok(toDoRepository.getTodo(todoId));
    }
}
