package com.example.codereview.service;

import com.example.codereview.model.Todo;
import com.example.codereview.model.TodoItem;
import com.example.codereview.repository.TodoRepositoryJdbc;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class TodoServiceImpl implements ToDoService {

    private final TodoRepositoryJdbc todoRepository;

    public TodoServiceImpl(final TodoRepositoryJdbc todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public Todo createTodo(final Todo todo) {

        try {
            return todoRepository.insertTodo(todo);
        }
        catch ( final DataAccessException e ) {
            // We don't care about database errors
        }

        return todo;
    }

    @Override
    public TodoItem createTodoItem(final UUID todoId, final TodoItem todoItem) {
        return null;
    }
}
