package com.example.codereview.service;

import com.example.codereview.model.Todo;
import com.example.codereview.model.TodoItem;

import java.util.UUID;

public interface ToDoService {

    Todo createTodo(Todo todo);

    TodoItem createTodoItem(UUID todoId, TodoItem todoItem);
}
