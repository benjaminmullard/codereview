package com.example.codereview.repository;

import com.example.codereview.model.Todo;
import com.example.codereview.model.TodoItem;

import java.util.List;
import java.util.UUID;

public interface ToDoRepository {

    Todo insertTodo(Todo todo);

    TodoItem createTodoItem(UUID todoId, TodoItem todoItem);

    List<Todo> getTodoList();

    Todo getTodo(UUID todoId);
}
