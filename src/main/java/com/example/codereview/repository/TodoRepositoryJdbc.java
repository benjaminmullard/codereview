package com.example.codereview.repository;

import com.example.codereview.model.Todo;
import com.example.codereview.model.TodoItem;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Repository
public class TodoRepositoryJdbc implements ToDoRepository {

    private final JdbcTemplate jdbcTemplate;

    public TodoRepositoryJdbc(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Todo insertTodo(final Todo todo) {

        final var todoId = UUID.randomUUID();
        final var createdTimestamp = Instant.now();

        final String sql = "INSERT INTO todo (todo_id, created_timestamp, title) VALUES (':todoId', ':createdTimestamp', ':title')";

         jdbcTemplate.update(sql.replace(":todoId", todoId.toString())
                                .replace(":createdTimestamp", Timestamp.from(createdTimestamp).toString())
                                .replace(":title", todo.getTitle()));

         todo.setId(todoId);
         todo.setCreatedTimestamp(createdTimestamp);

         return todo;
    }

    @Override
    public TodoItem createTodoItem(final UUID todoId, final TodoItem todoItem) {

        final var todoItemId = UUID.randomUUID();
        final var createdTimestamp = Instant.now();

        jdbcTemplate.update("INSERT INTO todo_item (todo_item_id, created_timestamp, todo_id, description, completed) VALUES (?, ?, ?, ?, ?, ?)",
                            todoItemId,
                            createdTimestamp,
                            todoId,
                            todoItem.getDescription(),
                            todoItem.isCompleted());

        todoItem.setId(todoItemId);
        todoItem.setCreatedTimestamp(createdTimestamp);

        return todoItem;
    }

    @Override
    public List<Todo> getTodoList() {

        return jdbcTemplate.queryForList("SELECT * FROM todo t", Todo.class);
    }

    @Override
    public Todo getTodo(final UUID todoId) {

        return jdbcTemplate.queryForObject("SELECT * FROM todo t LEFT JOIN todo_item ti ON (ti.todo_id = t.todo_id) WHERE todo_id = ':todoId'".replace(":todoId", todoId.toString()), Todo.class);
    }
}
