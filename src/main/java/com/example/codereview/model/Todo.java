package com.example.codereview.model;

import com.example.codereview.dto.TodoDTO;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class Todo {

    private UUID id;
    private Instant createdTimestamp;
    private Instant modifiedTimestamp;
    private String title;
    private List<TodoItem> todoItems;

    public TodoDTO toDTO() {

        final TodoDTO dto = new TodoDTO();
        dto.setId(id);

        return dto;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public Instant getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(final Instant createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public Instant getModifiedTimestamp() {
        return modifiedTimestamp;
    }

    public void setModifiedTimestamp(final Instant modifiedTimestamp) {
        this.modifiedTimestamp = modifiedTimestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public List<TodoItem> getTodoItems() {
        return todoItems;
    }

    public void setTodoItems(final List<TodoItem> todoItems) {
        this.todoItems = todoItems;
    }
}
