package com.example.codereview.model;

import java.time.Instant;
import java.util.UUID;

public class TodoItem {

    private UUID id;
    private Instant createdTimestamp;
    private Instant modifiedTimestamp;
    private String description;
    private boolean completed;

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public Instant getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(final Instant createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public Instant getModifiedTimestamp() {
        return modifiedTimestamp;
    }

    public void setModifiedTimestamp(final Instant modifiedTimestamp) {
        this.modifiedTimestamp = modifiedTimestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(final boolean completed) {
        this.completed = completed;
    }
}
