package com.example.codereview.dto;

import com.example.codereview.model.TodoItem;
import lombok.Data;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
public class TodoDTO {

    private UUID id;
    private Instant createdTimestamp;
    private Instant modifiedTimestamp;
    private String title;
    private List<TodoItem> todoItems;
}
