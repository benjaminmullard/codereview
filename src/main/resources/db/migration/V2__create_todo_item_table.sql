CREATE TABLE IF NOT EXISTS todo_item (
    todo_item_id UUID PRIMARY KEY NOT NULL,
    created_timestamp TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    modified_timestamp TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    todo_id UUID NOT NULL,
    description VARCHAR NOT NULL,
    completed BOOLEAN NOT NULL
)