CREATE TABLE IF NOT EXISTS todo (
    todo_id UUID PRIMARY KEY NOT NULL,
    created_timestamp TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    modified_timestamp TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    title VARCHAR NOT NULL
)
