package com.example.codereview.repository;

import com.example.codereview.model.TodoItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class TodoRepositoryJdbcTest {

    @InjectMocks
    private TodoRepositoryJdbc underTest;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Test
    public void testCreateTodoItem() {

        final var id = UUID.randomUUID();

        underTest.createTodoItem(id, new TodoItem());
    }
}
