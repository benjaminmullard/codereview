# Release Notes

## 1.0.0 (unreleased)

### Initial release

* ToDo endpoints to create, modify and get to-do lists and items
* Persistence for to-do data